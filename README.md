# Billy the Pill Dispenser

## introduction

This is the repository for the Billy the Pill Dispenser project. This project is part of the European Project Semester (EPS) at Instituto Superior de Engenharia do Porto (ISEP). The aim of the EPS is to work on a project that responds to a real and topical problem and to develop a number of skills, such as teamwork, communication and improving our English.

## What is Billy?

Billy is a friendly pill dispenser for children suffering from long-term illnesses, allergies or epilepsy. Billy makes it easier for children to take their medicine, reassures parents and ensures that they never forget to take their medicine.

For more details, please visit our [project wiki](https://www.eps2024-wiki2.dee.isep.ipp.pt/doku.php?id=report).

## Participants

Institution: Instituto Superior de Engenharia do Porto

Team N.2: Camelia

Students:

- Łukasz Borowski
- Tibo Clauwaert
- Lena Ehrenhofer
- Tamara Kronshagen
- Noé Oliveira
- Stijn Steyaert

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/tibo.clauwaert/billy-the-pill-dispenser.git
git branch -M main
git push -uf origin main
```
