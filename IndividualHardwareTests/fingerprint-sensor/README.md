# Fingerprint Sensor README

This repo contains two passes of code for the fingerprint sensor module. The code is based on the example code provided by Adafruit.

## Code Passes

1. **Enrollment Code**: This code is used to enroll a new fingerprint.

2. **Verification Code**: This code is used to verify the fingerprint.

## Pin Mapping

Here is the pin mapping for connecting the fingerprint sensor to the ESP32:

- Fingerprint Sensor (Adafruit): [Product Link](https://www.adafruit.com/product/4690#description)
- Pin 8 (Red - VCC) -> 3.3V
- Pin 7 (Black - TX) -> IO16 (UART2 RX)
- Pin 6 (Yellow - RX) -> IO14 (UART2 TX)
- Pin 5 (Green - GND) -> GND
