/*
  This file contains two passes of code
  1 - the first one is the code that is used to enroll a new fingerprint
  2 - the second one is the code that is used to verify the fingerprint

  the code is based on the example code provided by Adafruit

  Here is the pin mapping for the fingerprint sensor (https://www.adafruit.com/product/4690#description)

  fingerprint sensor -> ESP32
  pin 8 red (VCC) -> 3.3V
  pin 7 black (TX) -> IO16 (UART2 RX)
  pin 6 yellow (RX) -> IO14 (UART2 TX)
  pin 5 green (GND) -> GND

*/

// code for enrolling a new fingerprint

// #include <HardwareSerial.h>
// #include <Adafruit_Fingerprint.h>

// // Create an instance of the HardwareSerial class for UART2
// HardwareSerial mySerial(2); // UART2

// // Create an instance of the Adafruit_Fingerprint class
// Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

// uint8_t id;

// uint8_t getFingerprintEnroll();
// uint8_t getFingerprintID();

// void setup()
// {
//   Serial.begin(9600);
//   while (!Serial)
//     ; // For Yun/Leo/Micro/Zero/...
//   delay(100);
//   Serial.println("\n\nAdafruit Fingerprint sensor enrollment");

//   //   // Initialize UART2 with the configured pins
//   mySerial.begin(57600, SERIAL_8N1, 16, 14); // Adjust the baud rate if needed

//   // set the data rate for the sensor serial port
//   finger.begin(57600);

//   if (finger.verifyPassword())
//   {
//     Serial.println("Found fingerprint sensor!");
//   }
//   else
//   {
//     Serial.println("Did not find fingerprint sensor :(");
//     while (1)
//     {
//       delay(1);
//     }
//   }

//   Serial.println(F("Reading sensor parameters"));
//   finger.getParameters();
//   Serial.print(F("Status: 0x"));
//   Serial.println(finger.status_reg, HEX);
//   Serial.print(F("Sys ID: 0x"));
//   Serial.println(finger.system_id, HEX);
//   Serial.print(F("Capacity: "));
//   Serial.println(finger.capacity);
//   Serial.print(F("Security level: "));
//   Serial.println(finger.security_level);
//   Serial.print(F("Device address: "));
//   Serial.println(finger.device_addr, HEX);
//   Serial.print(F("Packet len: "));
//   Serial.println(finger.packet_len);
//   Serial.print(F("Baud rate: "));
//   Serial.println(finger.baud_rate);
// }

// uint8_t readnumber(void)
// {
//   uint8_t num = 0;

//   while (num == 0)
//   {
//     while (!Serial.available())
//       ;
//     num = Serial.parseInt();
//   }
//   return num;
// }

// void loop() // run over and over again
// {
//   Serial.println("Ready to enroll a fingerprint!");
//   Serial.println("Please type in the ID # (from 1 to 127) you want to save this finger as...");
//   id = readnumber();
//   if (id == 0)
//   { // ID #0 not allowed, try again!
//     return;
//   }
//   Serial.print("Enrolling ID #");
//   Serial.println(id);

//   while (!getFingerprintEnroll())
//     ;
// }

// uint8_t getFingerprintEnroll()
// {

//   int p = -1;
//   Serial.print("Waiting for valid finger to enroll as #");
//   Serial.println(id);
//   while (p != FINGERPRINT_OK)
//   {
//     p = finger.getImage();
//     switch (p)
//     {
//     case FINGERPRINT_OK:
//       Serial.println("Image taken");
//       break;
//     case FINGERPRINT_NOFINGER:
//       Serial.print(".");
//       break;
//     case FINGERPRINT_PACKETRECIEVEERR:
//       Serial.println("Communication error");
//       break;
//     case FINGERPRINT_IMAGEFAIL:
//       Serial.println("Imaging error");
//       break;
//     default:
//       Serial.println("Unknown error");
//       break;
//     }
//   }

//   // OK success!

//   p = finger.image2Tz(1);
//   switch (p)
//   {
//   case FINGERPRINT_OK:
//     Serial.println("Image converted");
//     break;
//   case FINGERPRINT_IMAGEMESS:
//     Serial.println("Image too messy");
//     return p;
//   case FINGERPRINT_PACKETRECIEVEERR:
//     Serial.println("Communication error");
//     return p;
//   case FINGERPRINT_FEATUREFAIL:
//     Serial.println("Could not find fingerprint features");
//     return p;
//   case FINGERPRINT_INVALIDIMAGE:
//     Serial.println("Could not find fingerprint features");
//     return p;
//   default:
//     Serial.println("Unknown error");
//     return p;
//   }

//   Serial.println("Remove finger");
//   delay(2000);
//   p = 0;
//   while (p != FINGERPRINT_NOFINGER)
//   {
//     p = finger.getImage();
//   }
//   Serial.print("ID ");
//   Serial.println(id);
//   p = -1;
//   Serial.println("Place same finger again");
//   while (p != FINGERPRINT_OK)
//   {
//     p = finger.getImage();
//     switch (p)
//     {
//     case FINGERPRINT_OK:
//       Serial.println("Image taken");
//       break;
//     case FINGERPRINT_NOFINGER:
//       Serial.print(".");
//       break;
//     case FINGERPRINT_PACKETRECIEVEERR:
//       Serial.println("Communication error");
//       break;
//     case FINGERPRINT_IMAGEFAIL:
//       Serial.println("Imaging error");
//       break;
//     default:
//       Serial.println("Unknown error");
//       break;
//     }
//   }

//   // OK success!

//   p = finger.image2Tz(2);
//   switch (p)
//   {
//   case FINGERPRINT_OK:
//     Serial.println("Image converted");
//     break;
//   case FINGERPRINT_IMAGEMESS:
//     Serial.println("Image too messy");
//     return p;
//   case FINGERPRINT_PACKETRECIEVEERR:
//     Serial.println("Communication error");
//     return p;
//   case FINGERPRINT_FEATUREFAIL:
//     Serial.println("Could not find fingerprint features");
//     return p;
//   case FINGERPRINT_INVALIDIMAGE:
//     Serial.println("Could not find fingerprint features");
//     return p;
//   default:
//     Serial.println("Unknown error");
//     return p;
//   }

//   // OK converted!
//   Serial.print("Creating model for #");
//   Serial.println(id);

//   p = finger.createModel();
//   if (p == FINGERPRINT_OK)
//   {
//     Serial.println("Prints matched!");
//   }
//   else if (p == FINGERPRINT_PACKETRECIEVEERR)
//   {
//     Serial.println("Communication error");
//     return p;
//   }
//   else if (p == FINGERPRINT_ENROLLMISMATCH)
//   {
//     Serial.println("Fingerprints did not match");
//     return p;
//   }
//   else
//   {
//     Serial.println("Unknown error");
//     return p;
//   }

//   Serial.print("ID ");
//   Serial.println(id);
//   p = finger.storeModel(id);
//   if (p == FINGERPRINT_OK)
//   {
//     Serial.println("Stored!");
//   }
//   else if (p == FINGERPRINT_PACKETRECIEVEERR)
//   {
//     Serial.println("Communication error");
//     return p;
//   }
//   else if (p == FINGERPRINT_BADLOCATION)
//   {
//     Serial.println("Could not store in that location");
//     return p;
//   }
//   else if (p == FINGERPRINT_FLASHERR)
//   {
//     Serial.println("Error writing to flash");
//     return p;
//   }
//   else
//   {
//     Serial.println("Unknown error");
//     return p;
//   }

//   return true;
// }

/////////////////////////////////////////////////////////////////
// code for scanning the fingerprint and finding the ID of the fingerprint

// Include the required libraries
#include <HardwareSerial.h>
#include <Adafruit_Fingerprint.h>

// Create an instance of the HardwareSerial class for UART2
HardwareSerial mySerial(2); // UART2

// Create an instance of the Adafruit_Fingerprint class
Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

int getFingerprintIDez();

void setup()
{
  Serial.begin(9600);

  while (!Serial)
    ; // For Yun/Leo/Micro/Zero/...
  delay(100);
  Serial.println("\n\nAdafruit finger detect test");

  //   // Initialize UART2 with the configured pins
  mySerial.begin(57600, SERIAL_8N1, 16, 14); // 16 RX, 14 TX

  // set the data rate for the sensor serial port
  finger.begin(57600);

  if (finger.verifyPassword())
  {
    Serial.println("Found fingerprint sensor!");
  }
  else
  {
    Serial.println("Did not find fingerprint sensor :(");
    while (1)
    {
      delay(1);
    }
  }

  finger.getTemplateCount();
  Serial.print("Sensor contains ");
  Serial.print(finger.templateCount);
  Serial.println(" templates");
  Serial.println("Waiting for valid finger...");
}

void loop() // run over and over again
{
  getFingerprintIDez();
  delay(50); // don't ned to run this at full speed.

  digitalWrite(5, HIGH);
}

uint8_t getFingerprintID()
{
  uint8_t p = finger.getImage();
  switch (p)
  {
  case FINGERPRINT_OK:
    Serial.println("Image taken");
    break;
  case FINGERPRINT_NOFINGER:
    Serial.println("No finger detected");
    return p;
  case FINGERPRINT_PACKETRECIEVEERR:
    Serial.println("Communication error");
    return p;
  case FINGERPRINT_IMAGEFAIL:
    Serial.println("Imaging error");
    return p;
  default:
    Serial.println("Unknown error");
    return p;
  }

  // OK success!

  p = finger.image2Tz();
  switch (p)
  {
  case FINGERPRINT_OK:
    Serial.println("Image converted");
    break;
  case FINGERPRINT_IMAGEMESS:
    Serial.println("Image too messy");
    return p;
  case FINGERPRINT_PACKETRECIEVEERR:
    Serial.println("Communication error");
    return p;
  case FINGERPRINT_FEATUREFAIL:
    Serial.println("Could not find fingerprint features");
    return p;
  case FINGERPRINT_INVALIDIMAGE:
    Serial.println("Could not find fingerprint features");
    return p;
  default:
    Serial.println("Unknown error");
    return p;
  }

  // OK converted!
  p = finger.fingerFastSearch();
  if (p == FINGERPRINT_OK)
  {
    Serial.println("Found a print match!");
  }
  else if (p == FINGERPRINT_PACKETRECIEVEERR)
  {
    Serial.println("Communication error");
    return p;
  }
  else if (p == FINGERPRINT_NOTFOUND)
  {
    Serial.println("Did not find a match");
    return p;
  }
  else
  {
    Serial.println("Unknown error");
    return p;
  }

  // found a match!
  Serial.print("Found ID #");
  Serial.print(finger.fingerID);
  Serial.print(" with confidence of ");
  Serial.println(finger.confidence);

  return finger.fingerID;
}

// returns -1 if failed, otherwise returns ID #
int getFingerprintIDez()
{
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)
    return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)
    return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)
    return -1;

  // found a match!

  Serial.print("Found ID #");
  Serial.print(finger.fingerID);
  Serial.print(" with confidence of ");
  Serial.println(finger.confidence);
  return finger.fingerID;
}