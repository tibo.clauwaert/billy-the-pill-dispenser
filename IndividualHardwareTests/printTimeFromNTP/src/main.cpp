#include <Arduino.h>

#include <WiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <TimeLib.h>

// Replace with your network credentials
const char *ssid = "PC-Tibo";
const char *password = "Tibo1234";

// Initialize WiFi and UDP
WiFiUDP ntpUDP;

// Timezone offset in seconds (for example: for CET it's 3600 seconds)
const long utcOffsetInSeconds = 3600;

// Initialize NTPClient
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

void setup()
{
  // Initialize Serial Monitor
  Serial.begin(115200);

  // Connect to WiFi
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");

  // Start NTPClient
  timeClient.begin();
}

void loop()
{
  // Update time from NTP server
  timeClient.update();

  // Get current epoch time and set the system time
  setTime(timeClient.getEpochTime());

  // Format the date and time as a string
  char timeString[25];
  sprintf(timeString, "%04d-%02d-%02d %02d:%02d:%02d", year(), month(), day(), hour(), minute(), second());

  // Print date and time to Serial Monitor
  Serial.println(timeString);

  // Wait for 1 second
  delay(1000);
}