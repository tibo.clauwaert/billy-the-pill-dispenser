#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_MCP23X17.h>

Adafruit_MCP23X17 mcp;
const int numSteps = 93; // Set the desired number of steps here

void setup()
{
  Serial.begin(9600);
  Wire.begin(2, 15);
  if (!mcp.begin_I2C())
  {
    Serial.println("MCP23X17 not found. Check wiring.");
    while (1)
      ;
  }
  Serial.println("MCP23X17 found!");
  for (int i = 0; i < 4; i++)
    mcp.pinMode(i, OUTPUT);
}

void loop()
{
  // Repeat the motor control sequence 93 times
  for (int step = 0; step < numSteps; step++)
  {
    // Calculate the current pin index based on the step number
    int currentPin = step % 4;

    // Set the appropriate pins for the motor control
    for (int i = 0; i < 4; i++)
    {
      if (i == currentPin)
      {
        mcp.digitalWrite(i, HIGH);
      }
      else
      {
        mcp.digitalWrite(i, LOW);
      }
    }

    // Add a delay for motor movement (adjust as needed)
    delay(10);
  }
  delay(1000); // One second delay before repeating the loop
}