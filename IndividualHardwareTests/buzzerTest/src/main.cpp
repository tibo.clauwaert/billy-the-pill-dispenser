#include <Arduino.h>
#include <Ticker.h>

const int buzzerPin = 8;
const int frequency = 2000; // 2 kHz frequency for the buzzer
const int ledChannel = 0;
const int resolution = 8;
bool buzzerState = false;

Ticker ticker; // Declare the Ticker object

void toggleBuzzer()
{
  buzzerState = !buzzerState;
  if (buzzerState)
  {
    ledcWriteTone(ledChannel, frequency);
  }
  else
  {
    ledcWriteTone(ledChannel, 0); // Stop the buzzer
  }
}

void setup()
{
  // Configure the PWM channel
  ledcSetup(ledChannel, frequency, resolution);
  // Attach the buzzer pin to the channel
  ledcAttachPin(buzzerPin, ledChannel);

  // Set up a timer to call toggleBuzzer every second
  ticker.attach(1, toggleBuzzer);
}

void loop()
{
  // No code needed here, everything is handled by the timer
}
