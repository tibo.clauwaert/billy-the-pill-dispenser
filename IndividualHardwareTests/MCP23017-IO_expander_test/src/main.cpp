// blinky code for MCP23017 IO expander

// pin0;  --> GPA0 (21) of the MCP23017
// pin1;  --> GPA1 (22) of the MCP23017
// pin2;  --> GPA2 (23) of the MCP23017
// pin3;  --> GPA3 (24) of the MCP23017
// pin4;  --> GPA4 (25) of the MCP23017
// pin5;  --> GPA5 (26) of the MCP23017
// pin6;  --> GPA6 (27) of the MCP23017
// pin7;  --> GPA7 (28) of the MCP23017
// pin8;  --> GPB0 (1) of the MCP23017
// pin9;  --> GPB1 (2) of the MCP23017
// pin10; --> GPB2 (3) of the MCP23017
// pin11; --> GPB3 (4) of the MCP23017
// pin12; --> GPB4 (5) of the MCP23017
// pin13; --> GPB5 (6) of the MCP23017
// pin14; --> GPB6 (7) of the MCP23017
// pin15; --> GPB7 (8) of the MCP23017

// #include <Wire.h>
#include <Adafruit_MCP23X17.h>

// // Maak een instantie van de MCP23X17 aan
// Adafruit_MCP23X17 mcp;

// void setup()
// {
//   // Initialiseer de I2C-bus
//   Wire.begin(2, 15); // 2-> SDA, 4-> SCL

//   // Start de seriële communicatie voor debugging
//   Serial.begin(9600);
//   delay(1000); // Wacht even voor de seriële communicatie

//   // Initialiseer de MCP23X17
//   if (!mcp.begin_I2C())
//   {
//     Serial.println("MCP23X17 niet gevonden. Controleer de bedrading.");
//     delay(500);
//   }
//   else
//   {
//     Serial.println("MCP23X17 gevonden!");
//   }

//   for (int i = 0; i <= 15; i++)
//   {
//     mcp.pinMode(i, OUTPUT);
//   }
// }

// void loop()
// {

//   for (int i = 0; i <= 15; i++)
//   {
//     Serial.print("Pin: ");
//     Serial.println(i);
//     mcp.digitalWrite(i, HIGH);
//   }

//   delay(1000);
//   for (int i = 0; i <= 15; i++)
//   {
//     Serial.print("Pin: ");
//     Serial.println(i);
//     mcp.digitalWrite(i, LOW);
//   }

//   delay(1000);
//   /*
//     // // Zet de LED aan
//     // mcp.digitalWrite(LED_PIN, HIGH);
//     // Serial.println("LED aan");
//     // delay(1000); // Wacht 1 seconde

//     // // Zet de LED uit
//     // mcp.digitalWrite(LED_PIN, LOW);
//     // Serial.println("LED uit");
//     // delay(1000); // Wacht 1 seconde
//   */
// }
///////////////////////////////////////////////////////////////

// #include <Arduino.h>
// #include <Adafruit_MCP23X17.h>

// #define LED_PIN 7    // MCP23XXX pin LED is attached to
// #define BUTTON_PIN 6 // MCP23XXX pin button is attached to

// Adafruit_MCP23X17 mcp;

// void setup()
// {
//   Serial.begin(9600);
//   // while (!Serial);

//   Wire.begin(2, 15); // 2-> SDA, 4-> SCL

//   // Initialiseer de MCP23X17
//   if (!mcp.begin_I2C())
//   {
//     Serial.println("MCP23X17 niet gevonden. Controleer de bedrading.");
//     delay(500);
//   }
//   else
//   {
//     Serial.println("MCP23X17 gevonden!");
//   }

//   // configure LED pin for output
//   mcp.pinMode(LED_PIN, OUTPUT);

//   // configure button pin for input with pull up
//   mcp.pinMode(BUTTON_PIN, INPUT);

//   Serial.println("Looping...");
// }

// void loop()
// {
//   if (mcp.digitalRead(BUTTON_PIN) == LOW)
//   {
//     mcp.digitalWrite(LED_PIN, HIGH);
//     Serial.println("Button pressed");
//   }
//   else
//   {
//     mcp.digitalWrite(LED_PIN, LOW);
//   }
// }

///////////////////////////////////////////////////////////////

// I2C Scanner code

#include <Arduino.h>
#include <Wire.h>

void setup()
{
  Wire.begin(2, 15);  // Initialize I2C communication
  Serial.begin(9600); // Start serial communication
  while (!Serial)
    ; // Wait for serial monitor to open
  Serial.println("\nI2C Scanner");
}

void loop()
{
  byte error, address;
  int devices = 0;

  Serial.println("Scanning...");

  for (address = 1; address < 127; address++)
  {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("Device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println();
      devices++;
    }
    else if (error == 4)
    {
      Serial.print("Unknown error at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }

  if (devices == 0)
    Serial.println("No I2C devices found\n");

  delay(5000); // Wait for 5 seconds before scanning again
}