#include <Arduino.h>
#include <Stepper.h>

// Define the number of steps per rotation:
const int stepsPerRevolution = 2048;

// Define the amount of steps per slot:
const int stepsPerSlot = 93;

/**
 * pinout:
 * IN1 -> 9
 * IN2 -> 8
 * IN3 -> 4
 * IN4 -> 5
 */

Stepper myStepper(stepsPerRevolution, 9, 4, 8, 5);

void setup()
{
  // put your setup code here, to run once:
  myStepper.setSpeed(5);
}

void loop()
{
  // put your main code here, to run repeatedly:
  myStepper.step(stepsPerSlot);
  delay(1000);
}