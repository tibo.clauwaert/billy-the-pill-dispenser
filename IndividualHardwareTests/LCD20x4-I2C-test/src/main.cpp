#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

/*
Connect the SDA pin of the LCD to the SDA (IO04) pin on the ESP32C3.
Connect the SCL pin of the LCD to the SCL (IO05) pin on the ESP32C3.
Connect the VCC pin of the LCD to the 3.3V pin on the ESP32C3.
Connect the GND pin of the LCD to the GND pin on the ESP32C3.
*/

// Set the LCD address to 0x27 for a 20 chars and 4 line display
LiquidCrystal_I2C lcd(0x27, 20, 4);

void setup()
{

  Wire.begin(4, 5);

  // Initialize the LCD
  lcd.init();
  // Turn on the backlight
  lcd.backlight();

  // Print a message to the LCD.
  lcd.setCursor(0, 0);
  lcd.print("Hello, World!");
  lcd.setCursor(0, 1);
  lcd.print("This is a 20x4 LCD");
  lcd.setCursor(0, 2);
  lcd.print("connected to an");
  lcd.setCursor(0, 3);
  lcd.print("ESP32C3 with I2C");
}

void loop()
{
  // You can add your code here to update the display as needed
}

/*
  below sketch is an I2C scanner that scans for devices on the I2C bus.

  use the following connections:
  - SDA pin of the LCD to the SDA (IO04) pin on the ESP32C3.
  - SCL pin of the LCD to the SCL (IO05) pin on the ESP32C3.
  - VCC pin of the LCD to the 3.3V pin on the ESP32C3.
  - GND pin of the LCD to the GND pin on the ESP32C3.
*/

// #include <Wire.h>
// #include <Arduino.h>

// void setup()
// {
//   Wire.begin(4, 5);     // Pas deze pinnen aan naar je configuratie
//   Serial.begin(115200); // Zorg ervoor dat deze baudrate overeenkomt met je seriële monitor instellingen
//   while (!Serial)
//     ; // Wacht tot het seriële port is geopend
//   Serial.println("\nI2C Scanner");
// }

// void loop()
// {
//   byte error, address;
//   int nDevices;

//   Serial.println("Scannen...");

//   nDevices = 0;
//   for (address = 1; address < 127; address++)
//   {
//     Wire.beginTransmission(address);
//     error = Wire.endTransmission();

//     if (error == 0)
//     {
//       Serial.print("I2C apparaat gevonden op adres 0x");
//       if (address < 16)
//       {
//         Serial.print("0");
//       }
//       Serial.print(address, HEX);
//       Serial.println("  !");

//       nDevices++;
//     }
//     else if (error == 4)
//     {
//       Serial.print("Onbekende fout op adres 0x");
//       if (address < 16)
//       {
//         Serial.print("0");
//       }
//       Serial.println(address, HEX);
//     }
//   }
//   if (nDevices == 0)
//   {
//     Serial.println("Geen I2C apparaten gevonden\n");
//   }
//   else
//   {
//     Serial.println("Klaar\n");
//   }

//   delay(5000); // 5 seconden pauze voordat opnieuw gescand wordt
// }
