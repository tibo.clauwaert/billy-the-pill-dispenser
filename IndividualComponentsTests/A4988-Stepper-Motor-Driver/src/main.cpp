#include <Arduino.h>
// test a stepper motor with a Pololu A4988 driver board, onboard led will flash at each step
// this version uses delay() to manage timing
// total number of steps for one revolution is 200
byte directionPin = 33;
byte stepPin = 32;
int stepsPerButtonPress = 25; // number of steps to move when button pressed
byte ledPin = 2;
int pulseWidthMicros = 20;   // microsecondo
int millisbetweenSteps = 10; // milliseconds - or try 100 for slower steps
void runMotor(byte direction, int steps);

void setup()
{
  Serial.begin(9600);
  Serial.println("Starting StepperTest");
  digitalWrite(ledPin, LOW);
  pinMode(directionPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(21, INPUT);
}

void loop()
{
  if (digitalRead(21) == HIGH)
  {
    runMotor(HIGH, stepsPerButtonPress);
  }
}

void runMotor(byte direction, int steps)
{
  digitalWrite(directionPin, direction);
  for (int n = 0; n < steps; n++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(pulseWidthMicros);
    digitalWrite(stepPin, LOW);
    delay(millisbetweenSteps);
    digitalWrite(ledPin, !digitalRead(ledPin));
  }
}