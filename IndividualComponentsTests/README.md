# Individual Components Tests

This folder contains the code to test and understand several components.
Please note that at the time of creating this code, we do not have possession of the physical hardware. therefore, we are utilizing a simulation tool for testing purposes.

## Components Included

1. **I2C LCD Screen**
2. **Servo Motor**
3. **A4988 Stepper Motor Driver**
4. **buzzer**
   - Other components may be included in the future.

## Simulation Tool

We are using the [Wokwi](https://wokwi.com/) simulation tool to emulate the behavior and interactions of the components listed above. This allows us to develop and test our code without access to the physical hardware.

## Note

Please ensure that you have the necessary dependencies and libraries installed for running the simulation successfully.
See the Wokwi [documentation](https://docs.wokwi.com/vscode/getting-started) ore, this handy [video](https://www.youtube.com/watch?v=9pTZL934k2s) as a manual to set up the simulation environment.
