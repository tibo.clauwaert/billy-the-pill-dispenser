#include <Arduino.h>
#include <ESP32Servo.h>

const int servoPin = 18;

Servo servo;

void setup()
{
  servo.attach(servoPin, 500, 2400);
}

int posOpen = 0;
int posClose = 180;

void loop()
{
  servo.write(posOpen);
  Serial.println("Open");

  delay(2000);

  servo.write(posClose);
  Serial.println("Close");

  delay(2000);
}