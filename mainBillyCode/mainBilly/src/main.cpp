#include <Arduino.h>
#include <Wire.h>
#include <WiFi.h>
#include <HardwareSerial.h>
#include <Adafruit_Fingerprint.h>
#include <Adafruit_MCP23X17.h>
#include <LiquidCrystal_I2C.h>
#include <ESP32Servo.h>
#include "time.h"
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include <SPIFFS.h>
#include "esp_camera.h"
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include <FS.h> //

#define buzzerPin 4

// Define the amount of steps per slot:
const int stepsPerSlot = 93;

const char *ntpServer = "pool.ntp.org";
const long gmtOffset_sec = 0;
// Timezone offset in seconds
const int daylightOffset_sec = 3600;

// Replace with your network credentials
const char *ssid = "PC-Tibo";
const char *password = "Tibo1234";

const char *serverName = "http://15.236.159.186/api/schedule";

#define photo_path "/image.png"

const char *api_endpoint = "http://15.236.159.186/api/gallery";

// OV2640 camera module pins (CAMERA_MODEL_AI_THINKER)
#define PWDN_GPIO_NUM 32
#define RESET_GPIO_NUM -1
#define XCLK_GPIO_NUM 0
#define SIOD_GPIO_NUM 26
#define SIOC_GPIO_NUM 27
#define Y9_GPIO_NUM 35
#define Y8_GPIO_NUM 34
#define Y7_GPIO_NUM 39
#define Y6_GPIO_NUM 36
#define Y5_GPIO_NUM 21
#define Y4_GPIO_NUM 19
#define Y3_GPIO_NUM 18
#define Y2_GPIO_NUM 5
#define VSYNC_GPIO_NUM 25
#define HREF_GPIO_NUM 23
#define PCLK_GPIO_NUM 22

unsigned long nextTime = 0;

bool TimeToDispense = false;

bool buzzed = false;

String timeUntil = "Getting next dispensing time...";

// temp variable for storing the HTTP response
String strHTTPres;

// JSON object for storing the parsed JSON response
JSONVar jsonArray;

// set up IO expander object
Adafruit_MCP23X17 mcp;

// Create an instance of the HardwareSerial class for UART2
HardwareSerial mySerial(2); // UART2 (fingerprint sensor)

// Create an instance of the Adafruit_Fingerprint class
Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

// Set the LCD address to 0x27 for a 20 chars and 4 line display
LiquidCrystal_I2C lcd(0x27, 20, 4);

// Servo objects
// Servo servo1;
// Servo servo2;

// Servo state variables
// bool servosOpen = false;

bool sendPhotoToAPI();

// put function declarations here:
void runStepper(int numSteps);

// gread fingerprint ID from sensor
int getFingerprintIDez();

// print dispensing message to LCD
void printDispeningMessage();

// buzzer function
void buzz();

void GetLocalTime();

// HTTP functions
String httpGETRequest(const char *serverName);
unsigned long findNextEpochTime(JSONVar jsonArray);
String getTimeUntil(unsigned long epochTime);

// function to toggle servos
// void toggleServos();

void captureSave_photo();

void setup()
{
  Serial.begin(9600);

  // set up I2C //
  Wire.begin(2, 15); // 2-> SDA, 15-> SCL

  // set up SPIFFS
  if (!SPIFFS.begin(true))
  {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else
  {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }

  // set up IO expander //
  if (!mcp.begin_I2C())
  {
    Serial.println("MCP23X17 not found. Check wiring.");
    delay(500);
  }
  else
  {
    Serial.println("MCP23X17 found!");
  }

  // set pin modes for IO expander
  // set pins SPA0-7 as outputs
  for (int i = 0; i <= 7; i++)
  {
    mcp.pinMode(i, OUTPUT);
  }
  // set pins SPB0-3 as inputs
  for (int i = 8; i <= 11; i++)
  {
    mcp.pinMode(i, INPUT);
  }

  // set up fingerprint sensor
  // Initialize UART2 with the configured pins
  mySerial.begin(57600, SERIAL_8N1, 16, 14); // 16 RX, 14 TX

  // set the data rate for the sensor serial port
  finger.begin(57600);

  if (finger.verifyPassword())
  {
    Serial.println("Found fingerprint sensor!");
  }
  else
  {
    Serial.println("Did not find fingerprint sensor :(");
    while (1)
    {
      delay(1);
    }
  }

  // set up display (LCD)

  // Initialize the LCD
  lcd.init();
  // Turn on the backlight
  lcd.backlight();

  // Print a message to the LCD.
  lcd.setCursor(0, 0);

  // set up buzzer
  pinMode(buzzerPin, OUTPUT);

  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

  // Set up servos
  // Allow allocation of all timers
  // ESP32PWM::allocateTimer(0);
  // ESP32PWM::allocateTimer(1);
  // ESP32PWM::allocateTimer(2);
  // ESP32PWM::allocateTimer(3);

  // servo1.setPeriodHertz(50); // Standard 50hz servo
  // servo2.setPeriodHertz(50); // Standard 50hz servo

  // pinMode(13, OUTPUT);

  // Attach servos to pins
  // servo1.attach(12, 500, 2400);
  // servo2.attach(13, 500, 2400);

  // Initialize servos to closed position
  // servo1.write(0);
  // servo2.write(0);

  // Connect to WiFi
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");

  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  // Turn-off the 'brownout detector'
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);

  // OV2640 camera module
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sccb_sda = SIOD_GPIO_NUM; // Replace pin_sscb_sda with pin_sccb_sda
  config.pin_sccb_scl = SIOC_GPIO_NUM; // Replace pin_sscb_scl with pin_sccb_scl
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;

  // if (psramFound())
  // {
  //   config.frame_size = FRAMESIZE_UXGA;
  //   config.jpeg_quality = 10;
  //   config.fb_count = 2;
  // }
  // else
  // {
  //   config.frame_size = FRAMESIZE_SVGA;
  //   config.jpeg_quality = 12;
  //   config.fb_count = 1;
  // }
  // esp_err_t err = esp_camera_init(&config);
  // if (err != ESP_OK)
  // {
  //   Serial.printf("Camera init failed with error 0x%x", err);
  //   ESP.restart();
  // }

  buzz();
}

void loop()
{
  Serial.println("Waiting for fingerprint...");
  int id = getFingerprintIDez();
  Serial.println("Fingerprint read!");
  Serial.println("ID: " + String(id));

  if (TimeToDispense)
  {
    if (!buzzed)
    {
      buzz();
      buzzed = true;
    }
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Time to take your");
    lcd.setCursor(0, 1);
    lcd.print("medicine!");
    lcd.setCursor(0, 2);
    lcd.print("Please scan finger.");
    if (id == 2)
    {
      printDispeningMessage();
      runStepper(stepsPerSlot);
      lcd.setCursor(0, 2);
      lcd.print("Dispensed!");
      TimeToDispense = false;
      delay(2000);
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Picture time!");
      lcd.setCursor(0, 1);
      lcd.print("Smile in:");
      lcd.setCursor(0, 2);
      for (int i = 5; i >= 1; i--)
      {
        lcd.print(i);
        delay(1000);
        lcd.print("  "); // clear the line by printing 20 spaces
      }
      lcd.print("Smile!");
      delay(1000);
      lcd.clear();

      // digitalWrite(buzzerPin, HIGH);
      captureSave_photo();
      // delay(10);
      // digitalWrite(buzzerPin, LOW);
      if (sendPhotoToAPI())
      {
        Serial.println("Photo sent to API successfully.");
      }
      else
      {
        Serial.println("Failed to send photo to API.");
      }
    }
  }
  else
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(timeUntil);
    lcd.setCursor(0, 3);
    lcd.print("parent can access");
  }

  if (id == 1)
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Welcome, parent!");
    // toggleServos();
    delay(1000);
  }
  if (id == -1)
  {
    Serial.println("Fingerprint not recognized.");
  }

  // get the current time
  GetLocalTime();

  // Get the JSON data from the server
  strHTTPres = httpGETRequest(serverName);
  Serial.println("HTTP response:");
  // Serial.println(strHTTPres);

  // parse the JSON data
  JSONVar jsonArray = JSON.parse(strHTTPres);

  Serial.println("Parsed JSON Array:");
  // Serial.println(jsonArray);

  // logic to dispense at the rice time
  if (time(nullptr) >= nextTime && nextTime != 0)
  {
    TimeToDispense = true;
    Serial.println("Time to dispense!");
  }

  // Find the next epoch time from the JSON array
  nextTime = findNextEpochTime(jsonArray);
  Serial.print("Next time: ");
  Serial.println(nextTime);

  // Get the time until the next epoch time
  timeUntil = getTimeUntil(nextTime);
  Serial.print("Time until: ");
  Serial.println(timeUntil);

  delay(1000);
}

// Function to run the stepper motor for a specified number of steps
void runStepper(int numSteps)
{
  // Repeat the motor control sequence for the specified number of steps
  for (int step = 0; step < numSteps; step++)
  {
    // Calculate the current pin index based on the step number
    int currentPin = step % 4;

    // Set the appropriate pins for the motor control
    for (int i = 0; i < 4; i++)
    {
      if (i == currentPin)
      {
        mcp.digitalWrite(i, HIGH);
      }
      else
      {
        mcp.digitalWrite(i, LOW);
      }
    }
    lcd.clear();
    lcd.setCursor(0, 1);
    // lcd.print("cuttent time: ");
    // lcd.setCursor(0, 2);
    // Print the time to LCD
    // time_t now = time(nullptr);
    // struct tm *timeinfo;
    // timeinfo = localtime(&now);
    // char timeStr[9];
    // strftime(timeStr, sizeof(timeStr), "%H:%M:%S", timeinfo);
    // lcd.print(timeStr);

    // Add a delay for motor movement (adjust as needed)
    delay(5);
  }
}

// Function to get the fingerprint ID from the sensor
// returns -1 if failed, otherwise returns ID #
int getFingerprintIDez()
{
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)
    return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)
    return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)
    return -1;

  // found a match!

  Serial.print("Found ID #");
  Serial.print(finger.fingerID);
  Serial.print(" with confidence of ");
  Serial.println(finger.confidence);
  return finger.fingerID;
}

void printDispeningMessage()
{
  // Print a message to the LCD.
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Dispensing...");
  lcd.setCursor(0, 1);
  lcd.print("Please wait...");
}

void buzz()
{
  // Notes for an energizing "call to action" melody
  int melody[] = {
      440, 494, 523, 494, 440, 659, 587, 523,
      494, 440, 880, 784, 659, 523, 494};

  // Note durations: 4 = quarter note, 8 = eighth note, etc.
  int noteDurations[] = {
      8, 8, 4, 8, 8, 4, 8, 8,
      8, 8, 4, 8, 8, 4, 2};

  for (int thisNote = 0; thisNote < 15; thisNote++)
  {
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(buzzerPin, melody[thisNote], noteDuration);

    // To distinguish the notes, set a minimum time between them.
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);

    // Stop the tone playing:
    noTone(buzzerPin);
  }

  Serial.println("Call to action melody played!");
}

// Function to toggle the servos between 0 and 90 degrees
// void toggleServos()
// {
//   if (servosOpen)
//   {
//     servo1.write(0);
//     // servo2.write(0);
//     servosOpen = false;
//     lcd.setCursor(0, 2);
//     lcd.print("lock closed.");
//     Serial.println("Servos closed.");
//   }
//   else
//   {
//     servo1.write(90);
//     // servo2.write(180);
//     servosOpen = true;
//     lcd.setCursor(0, 2);
//     lcd.print("lock opened.");
//     Serial.println("Servos opened.");
//   }
// }

// function to print the current time to the lcd
void GetLocalTime()
{
  struct tm timeinfo;

  if (!getLocalTime(&timeinfo))
  {
    Serial.println("Failed to obtain time");
    return;
  }
  else
  {
    char timeStr[30];
    strftime(timeStr, sizeof(timeStr), "%A, %B %d %Y %H:%M:%S", &timeinfo);
    Serial.print("Formatted Time: ");
    Serial.println(timeStr);
    Serial.print("Epoch Time: ");
    Serial.println(mktime(&timeinfo));
  }
}

String httpGETRequest(const char *serverName)
{
  WiFiClient client;
  HTTPClient http;

  http.begin(client, serverName);

  int httpResponseCode = http.GET();

  String payload = "{}";

  if (httpResponseCode > 0)
  {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else
  {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }

  http.end();

  return payload;
}

unsigned long findNextEpochTime(JSONVar jsonArray)
{
  unsigned long currentTime = time(nullptr);
  unsigned long nextTime = ULONG_MAX;

  for (int i = 0; i < jsonArray.length(); i++)
  {
    JSONVar item = jsonArray[i];
    unsigned long epochTime = (unsigned long)item["date"];
    if (epochTime > currentTime && epochTime < nextTime)
    {
      nextTime = epochTime;
    }
  }

  return nextTime;
}

String getTimeUntil(unsigned long epochTime)
{
  unsigned long currentTime = time(nullptr);
  unsigned long timeDifference = epochTime - currentTime;

  // Calculate the time components
  unsigned long days = timeDifference / 86400;
  unsigned long hours = (timeDifference % 86400) / 3600;
  unsigned long minutes = (timeDifference % 3600) / 60;
  unsigned long seconds = timeDifference % 60;

  // Create the time string
  String timeString = "";
  if (days > 0)
  {
    timeString += String(days) + " days ";
  }
  if (hours > 0)
  {
    timeString += String(hours) + " hours ";
  }
  if (minutes > 0)
  {
    timeString += String(minutes) + " minutes ";
  }
  if (seconds > 0)
  {
    timeString += String(seconds) + " seconds";
  }

  return timeString;
}

// Check if photo capture was successful
bool check_photo(fs::FS &fs)
{
  File f_pic = fs.open(photo_path);
  unsigned int pic_sz = f_pic.size();
  return (pic_sz > 100);
}

// Capture Photo and Save it to SPIFFS
void captureSave_photo(void)
{
  camera_fb_t *fb = NULL;
  bool ok = 0;

  do
  {
    Serial.println("ESP32-CAMP capturing photo...");

    fb = esp_camera_fb_get();
    if (!fb)
    {
      Serial.println("Failed");
      return;
    }

    Serial.printf("Picture file name: %s\n", photo_path);
    File file = SPIFFS.open(photo_path, FILE_WRITE);
    if (!file)
    {
      Serial.println("Failed to open file in writing mode");
    }
    else
    {
      file.write(fb->buf, fb->len);
      Serial.print("The picture has been saved in ");
      Serial.print(photo_path);
      Serial.print(" - Size: ");
      Serial.print(file.size());
      Serial.println(" bytes");
    }
    file.close();
    esp_camera_fb_return(fb);

    ok = check_photo(SPIFFS);
  } while (!ok);
}

// Send Photo to API
bool sendPhotoToAPI()
{
  File file = SPIFFS.open(photo_path, FILE_READ);
  if (!file)
  {
    Serial.println("Failed to open file for reading");
    return false;
  }

  HTTPClient http;
  WiFiClient wifiClient;
  http.begin(wifiClient, api_endpoint);

  String boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW";
  String bodyStart = "--" + boundary + "\r\nContent-Disposition: form-data; name=\"image\"; filename=\"image.jpg\"\r\nContent-Type: image/jpg\r\n\r\n";
  String bodyEnd = "\r\n--" + boundary + "--\r\n";

  int contentLength = bodyStart.length() + file.size() + bodyEnd.length();

  http.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
  http.addHeader("Content-Length", String(contentLength));

  int httpResponseCode = http.POST([&]()
                                   {
    String body = bodyStart;
    size_t fileSize = file.size();
    std::unique_ptr<uint8_t[]> buffer(new uint8_t[fileSize]);
    file.read(buffer.get(), fileSize);
    body += String((char*)buffer.get(), fileSize);
    body += bodyEnd;
    return body; }());

  file.close();

  if (httpResponseCode == 200)
  {
    String response = http.getString();
    Serial.println("HTTP Response code: " + String(httpResponseCode));
    Serial.println("Response: " + response);
    return true;
  }
  else
  {
    Serial.println("HTTP Response code: " + String(httpResponseCode));
    Serial.println("Error: " + http.errorToString(httpResponseCode));
    return false;
  }
}